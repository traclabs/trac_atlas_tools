#include <cloud_aggregator/cloud_aggregator.hpp>

namespace simple_aggregator
{

  SimpleAggregator::SimpleAggregator(const ros::NodeHandle& nh) :
    m_nh(nh)
  {

    m_nh.param("cloud_in", m_cloud_in, std::string("cloud_in"));
    m_nh.param("cloud_out", m_cloud_out, std::string("cloud_out"));
    m_nh.param("base_frame", m_base_frame, std::string("/ground"));
 
    m_nh.param("aggregated_points", m_aggregated_points, 100000);
    m_nh.param("spin_rate", m_rate, 100.);

    //m_cloud_sub = m_nh.subscribe<sensor_msgs::PointCloud2>("/sparsified_cloud_stream", 100, &SimpleAggregator::cloud_callback, this);
    m_cloud_sub = m_nh.subscribe<sensor_msgs::PointCloud2>(m_cloud_in, 100, &SimpleAggregator::cloud_callback, this);
    m_cloud_pub = m_nh.advertise<sensor_msgs::PointCloud2>(m_cloud_out, 1);
    m_filter_sub = m_nh.subscribe<std_msgs::Float32>("set_filter_radius", 1, &SimpleAggregator::radius_callback, this);
    m_reset_srv = m_nh.advertiseService("reset_aggregator", &SimpleAggregator::reset_aggregator, this);
    m_radius_clip = 10.0;
    m_initialized = false;

  }

  SimpleAggregator::~SimpleAggregator(){}

  bool SimpleAggregator::reset_aggregator(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
  {
    m_buffer->clear();
    ROS_INFO("Aggregator reset!");
    return true;
  }

  void SimpleAggregator::cloud_callback(const sensor_msgs::PointCloud2ConstPtr& msg)
  {       
    if(!m_initialized){
      m_point_step = msg->point_step;
      m_frame_id = msg->header.frame_id;
      m_is_bigendian = msg->is_bigendian;
      m_buffer = new boost::circular_buffer<uint8_t>(m_point_step*m_aggregated_points);
      m_fields = msg->fields;
      m_initialized = true;
    }
    if(m_point_step != msg->point_step || m_frame_id != msg->header.frame_id){
      ROS_WARN_THROTTLE(5.0, "SimpleAggregator: experiencing inconsistent point steps and/or frame_ids");
      return;
    }
    if(msg->data.size()%m_point_step != 0){
      ROS_WARN_THROTTLE(5.0, "SimpleAggregator: new data received length(%u) inconsistent with point step(%u)", (uint)msg->data.size(), m_point_step);
      return;
    }
    for(auto it = msg->data.begin(); it != msg->data.end(); ++it)
      m_buffer->push_back(*it);
  }

  void SimpleAggregator::radius_callback(const std_msgs::Float32ConstPtr& msg) 
  {
    m_radius_clip = msg->data;
    ROS_INFO("SETTING CLOUD FILTER = %f", m_radius_clip);
  }


  void SimpleAggregator::spin()
  {

    ROS_INFO("SimpleAggregator started");
    ros::Rate rate(m_rate);
    while(ros::ok()){
   
      if(m_initialized){
        sensor_msgs::PointCloud2 pc_in, pc_transformed, pc_filtered;
        tf::StampedTransform transform;

        pc_in.header.frame_id = m_frame_id;
        pc_in.height = 1;
        pc_in.point_step = m_point_step;
        pc_in.is_bigendian = m_is_bigendian;
        pc_in.row_step = m_buffer->size();
        pc_in.width = pc_in.row_step/pc_in.point_step;
        pc_in.is_dense = true;
        pc_in.fields = m_fields;
        pc_in.data = std::vector<uint8_t>(m_buffer->begin(),m_buffer->end());

        m_tf_listener.lookupTransform(m_frame_id, m_base_frame, ros::Time(0), transform);
        pcl_ros::transformPointCloud(m_base_frame, transform.inverse(), pc_in, pc_transformed);

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);

        //pcl::fromROSMsg(pc_transformed, *cloud);
        pcl::fromROSMsg(pc_transformed, *cloud_filtered);

        /*
        pcl::PassThrough<pcl::PointXYZ> pass;
    
        pass.setInputCloud (cloud);
        pass.setFilterFieldName ("x");
        pass.setFilterLimits (-m_radius_clip, m_radius_clip);
        pass.filter (*cloud_filtered);

        pass.setInputCloud (cloud_filtered);
        pass.setFilterFieldName ("y");
        pass.setFilterLimits (-m_radius_clip, m_radius_clip);
        pass.filter (*cloud_filtered);

        pass.setInputCloud (cloud_filtered);
        pass.setFilterFieldName ("z");
        pass.setFilterLimits (-std::numeric_limits<float>::max(), 2.1);
        pass.filter (*cloud_filtered);
*/
        pcl::toROSMsg(*cloud_filtered, pc_transformed);

        tf::StampedTransform transform_back;
        m_tf_listener.lookupTransform(m_base_frame, m_frame_id, ros::Time(0), transform_back);
        pcl_ros::transformPointCloud(m_frame_id, transform_back.inverse(), pc_transformed, pc_filtered);

        m_cloud_pub.publish(pc_filtered);
      }      
      ros::spinOnce();
      rate.sleep();
    }
  }
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "simple_aggregator");
  ros::NodeHandle nh("~");

  simple_aggregator::SimpleAggregator node(nh);
  node.spin();

  return 0;
}
