#ifndef CLOUD_AGGREGATOR_HPP
#define CLOUD_AGGREGATOR_HPP

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Float32.h>
#include <boost/circular_buffer.hpp>

#include <pcl/conversions.h> 
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl_ros/transforms.h> 

#include <tf/transform_listener.h>

namespace simple_aggregator
{

  class SimpleAggregator{

  public:
    SimpleAggregator(const ros::NodeHandle& nh);
    ~SimpleAggregator();
    void spin();
  private:

    ros::NodeHandle m_nh;
    ros::Publisher m_cloud_pub;
    ros::Subscriber m_cloud_sub;
    ros::Subscriber m_filter_sub;
    ros::ServiceServer m_reset_srv;
    
    tf::TransformListener m_tf_listener;

    bool m_initialized;
    bool m_is_bigendian;
    int m_aggregated_points;
    double m_rate;
    double m_radius_clip;
    std::string m_base_frame;
    std::string m_cloud_in;
    std::string m_cloud_out;

    uint m_point_step;
    std::vector<sensor_msgs::PointField> m_fields;
    std::string m_frame_id;
    boost::circular_buffer<uint8_t> * m_buffer;
    
    bool reset_aggregator(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
    void cloud_callback(const sensor_msgs::PointCloud2ConstPtr& msg);
    void radius_callback(const std_msgs::Float32ConstPtr& msg);
  };
}


#endif //CLOUD_AGGREGATOR_HPP
