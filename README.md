### Start up Instructions ###

* Install the following packages:
* [https://bitbucket.org/ihmcrobotics/ihmc_ros](https://bitbucket.org/ihmcrobotics/ihmc_ros)
* [https://bitbucket.org/traclabs/robot_interaction_tools](https://bitbucket.org/traclabs/robot_interaction_tools) (branch: **traclabs-devel**)
* [https://bitbucket.org/traclabs/navigation_planner](https://bitbucket.org/traclabs/navigation_planner) (branch: **develop**)

Additional instructions for setting up the IHMC SCS simulator from atlas are [here](https://bytebucket.org/traclabs/trac_atlas_tools/raw/37f9c591d62bde9ea88fd61cd9384e2bc02281f2/trac_atlas/doc/scs_setup.pdf).

```
#!bash
$ roslaunch trac_atlas atlas_scs.launch
```

To run just the navigation/footstep tools: 
```
#!bash
$ roslaunch trac_atlas atlas_navigation.launch 
```

To run the navigation **and** arm interaction tools: 
```
#!bash
$ roslaunch trac_atlas atlas_interaction.launch 
```