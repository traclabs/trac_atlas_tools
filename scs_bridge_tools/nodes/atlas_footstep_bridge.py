#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest("scs_bridge_tools")
import tf 
import PyKDL as kdl 

from ihmc_msgs.msg import FootstepDataListMessage, FootstepDataMessage

from visualization_msgs.msg  import MarkerArray

class FootstepBridge(object) :

  def __init__(self) :

    self.footstep_sub = rospy.Subscriber("/atlas_navigation/footstep_cmd", MarkerArray, self.footstep_callback)
    self.footstep_cmd = rospy.Publisher("/ihmc_ros/atlas/control/footstep_list", FootstepDataListMessage, queue_size=1)


  def footstep_callback(self, data) :

    footsteps = FootstepDataListMessage()
    footsteps.transfer_time = 2.
    footsteps.swing_time = 2.

    for i in range(len(data.markers)) :

      footstep = FootstepDataMessage()

      if "left" in data.markers[i].ns :
        footstep.robot_side = FootstepDataMessage.LEFT
      else :
        footstep.robot_side = FootstepDataMessage.RIGHT

      footstep.location.x = data.markers[i].pose.position.x
      footstep.location.y = data.markers[i].pose.position.y
      footstep.location.z = data.markers[i].pose.position.z
      footstep.orientation.x = data.markers[i].pose.orientation.x
      footstep.orientation.y = data.markers[i].pose.orientation.y
      footstep.orientation.z = data.markers[i].pose.orientation.z
      footstep.orientation.w = data.markers[i].pose.orientation.w

      footstep.trajectory_type = FootstepDataMessage.BASIC
      footstep.swing_height = 0.15

      footsteps.footstep_data_list.append(footstep)
    

    print footsteps
    self.footstep_cmd.publish(footsteps)


if __name__ == "__main__":
    rospy.init_node('atlas_footstep_bridge')


    bridge = FootstepBridge()

    r = rospy.Rate(100.0)
    while not rospy.is_shutdown():
      r.sleep()
