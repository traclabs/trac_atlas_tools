#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest("scs_bridge_tools")
import tf 
import PyKDL as kdl 
from math import fabs

from trajectory_msgs.msg import JointTrajectory
from control_msgs.msg import FollowJointTrajectoryActionGoal
from ihmc_msgs.msg import ArmJointTrajectoryPacketMessage, JointTrajectoryPointMessage, HeadOrientationPacketMessage

class ArmTrajectoryBridge(object) :


  def __init__(self) :
    
    self.left_arm_sub = rospy.Subscriber("left_arm_controller/goal", FollowJointTrajectoryActionGoal, self.left_arm_callback)
    self.right_arm_sub = rospy.Subscriber("right_arm_controller/goal", FollowJointTrajectoryActionGoal, self.right_arm_callback)
    self.neck_sub = rospy.Subscriber("neck_controller/goal", FollowJointTrajectoryActionGoal, self.neck_callback)
    
    self.arm_cmd = rospy.Publisher("/ihmc_ros/atlas/control/arm_joint_trajectory", ArmJointTrajectoryPacketMessage, queue_size=1)
    self.neck_cmd = rospy.Publisher("/ihmc_ros/atlas/control/head_orientation", HeadOrientationPacketMessage, queue_size=1)

    # joint order shoulder pitch, shoulder roll, elbow pitch, elbow roll, wrist pitch, wrist roll
    self.l_arm_joint_names = ['l_arm_shz', 'l_arm_shx', 'l_arm_ely', 'l_arm_elx', 'l_arm_wry', 'l_arm_wrx', 'l_arm_wry2']
    self.r_arm_joint_names = ['r_arm_shz', 'r_arm_shx', 'r_arm_ely', 'r_arm_elx', 'r_arm_wry', 'r_arm_wrx', 'r_arm_wry2']


  def left_arm_callback(self, data) :

    joint_map = {}
    for n in self.l_arm_joint_names :
      joint_map[n] = data.goal.trajectory.joint_names.index(n)

    traj = self.translate_traj_to_ihmc_msg(data.goal.trajectory.points, joint_map, self.l_arm_joint_names)
    traj.robot_side = traj.LEFT

    self.arm_cmd.publish(traj)


  def right_arm_callback(self, data) :

    joint_map = {}
    for n in self.r_arm_joint_names :
      joint_map[n] = data.goal.trajectory.joint_names.index(n)

    traj = self.translate_traj_to_ihmc_msg(data.goal.trajectory.points, joint_map, self.r_arm_joint_names)
    traj.robot_side = traj.RIGHT

    self.arm_cmd.publish(traj)
  

  def neck_callback(self, data) :

    points = data.goal.trajectory.points
    
    last_idx = len(points)-1
    yaw = points[last_idx].positions[0]

    q = kdl.Rotation.RPY(0,yaw,0).GetQuaternion()
    
    goal = HeadOrientationPacketMessage()
    goal.orientation.x = q[0]
    goal.orientation.y = q[1]
    goal.orientation.z = q[2]
    goal.orientation.w = q[3]
    goal.trajectory_time = 1

    self.neck_cmd.publish(goal)


  def traj_callback(self, data) :

    traj = JointTrajectory()
    traj = data.goal.trajectory
    self.arm_cmd.publish(traj)


  def translate_traj_to_ihmc_msg(self, points, joint_map, joint_names) :

    traj = ArmJointTrajectoryPacketMessage()

    # MoveIt! is incorrectly returning all 0's for 1st point. why is it dumb? 
    if len(points) > 1 :
      points[0].positions = points[1].positions
      points[0].velocities = points[1].velocities
      
    last_idx = len(points)-1
    des_vel = []
    t_final = points[last_idx].time_from_start.to_sec()

    for point in points :
      for n in joint_names :
        dpos = fabs(points[last_idx].positions[joint_map[n]] - points[0].positions[joint_map[n]])
        des_vel.append(dpos/t_final)

    for point in points :
      j = JointTrajectoryPointMessage()
      for n in joint_names :
        j.positions.append(point.positions[joint_map[n]])
        j.velocities.append(des_vel[joint_map[n]])
      j.time =  point.time_from_start.to_sec()*1.0
      traj.trajectory_points.append(j)
      
    return traj


if __name__ == "__main__":
    rospy.init_node('atlas_moveit_bridge')


    bridge = ArmTrajectoryBridge()

    r = rospy.Rate(100.0)
    while not rospy.is_shutdown():
      r.sleep()
