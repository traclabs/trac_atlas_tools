#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest("scs_bridge_tools")

from laser_assembler.srv import *
from sensor_msgs.msg import PointCloud2 

class LaserAggregator(object) :

  def __init__(self) :

    self.cloud_pub = rospy.Publisher("/atlas/laser_cloud", PointCloud2, queue_size=1)

  def aggregate(self) :

    rospy.wait_for_service("assemble_scans")

    try:
      assemble_scans = rospy.ServiceProxy('assemble_scans2', AssembleScans2)
      resp = assemble_scans(rospy.Time(0,0), rospy.get_rostime())
      self.cloud_pub.publish(resp.cloud)
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e


if __name__ == "__main__":
    rospy.init_node('laser_aggregator')

    aggregator = LaserAggregator()

    r = rospy.Rate(10.0)
    while not rospy.is_shutdown():
      aggregator.aggregate()
      r.sleep()
