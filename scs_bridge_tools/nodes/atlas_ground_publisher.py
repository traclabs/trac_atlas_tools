#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest("scs_bridge_tools")
import tf 
import PyKDL as kdl 
import tf2_ros

if __name__ == "__main__":

    rospy.init_node('atlas_ground_publisher')
  
    tf_broadcaster = tf.TransformBroadcaster()

    tf_buffer = tf2_ros.Buffer()  
    tf_listener = tf2_ros.TransformListener(tf_buffer)
    
    p_frame = "pelvis"
    l_foot_frame = "l_foot_center"
    r_foot_frame = "r_foot_center"
    g_frame = "ground"
    w_frame = "world"

    r = rospy.Rate(50.0)
    while not rospy.is_shutdown():

      try: 
        # print "looking up transform from: ", w_frame


        l_trans = tf_buffer.lookup_transform(w_frame, l_foot_frame, rospy.Time(0))
        r_trans = tf_buffer.lookup_transform(w_frame, r_foot_frame, rospy.Time(0))
         
        l_rpy = kdl.Rotation.Quaternion(l_trans.transform.rotation.x,l_trans.transform.rotation.y,l_trans.transform.rotation.z,l_trans.transform.rotation.w).GetRPY()
        r_rpy = kdl.Rotation.Quaternion(r_trans.transform.rotation.x,r_trans.transform.rotation.y,r_trans.transform.rotation.z,r_trans.transform.rotation.w).GetRPY()
           
        trans = [0]*3
        rpy = [0]*3

        trans[0] = (l_trans.transform.translation.x+r_trans.transform.translation.x)/2.
        trans[1] = (l_trans.transform.translation.y+r_trans.transform.translation.y)/2.
        trans[2] = (l_trans.transform.translation.z+r_trans.transform.translation.z)/2.

        for i in range(3) :
          rpy[i] = (l_rpy[i]+r_rpy[i])/2.
        rot = kdl.Rotation.RPY(rpy[0],rpy[1],rpy[2]).GetQuaternion()
          
        # print trans
        # print rpy
        
        try :
            tf_broadcaster.sendTransform(trans, rot, rospy.Time.now(), g_frame, w_frame)
        except :
            rospy.logdebug("atlas_ground_publisher() -- could not broadcast post")

      except Exception as e:
        print e

      r.sleep()
